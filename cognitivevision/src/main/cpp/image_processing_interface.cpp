#include <jni.h>
#include <functional>
#include <android/log.h>
#include <android/bitmap.h>

#include "libIPE.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace cv;
cm::simple_finder_pipeline sfp;

extern "C" {
JNIEXPORT void JNICALL Java_cm_cognitivevision_JNIInterface_TestCV(JNIEnv *env, jobject) {
    __android_log_write(ANDROID_LOG_ERROR, "CMVP", "Called JNI");
}

JNIEXPORT void JNICALL Java_cm_cognitivevision_CognitiveVisionInterface_cognitiveVisionFinder(JNIEnv *env, jobject, jlong addrRgba) {
    Mat&  src  = *(Mat*)addrRgba;

    sfp.set_input_image(src);
    sfp.set_output_image(src);
    sfp.process_pipeline();

    src = sfp.get_output_image();
}

JNIEXPORT void JNICALL Java_cm_cognitivevision_CognitiveVisionInterface_setConfiguration(JNIEnv *env, jobject, jstring json_buffer) {
    const char * json = env->GetStringUTFChars( json_buffer, NULL) ;
    sfp.set_pipeline_configuration(json);
}

}