#include "libIPE.hpp"

// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/geometric_types.cpp
// //////////////////////////////////////////////////////////////////////


namespace cm {

bool are_circles_concentric(circle_i a, circle_i b)
{
    return ( ( std::abs(a.center.x - b.center.x) < b.radius ) &&
             ( std::abs(a.center.y - b.center.y) < b.radius ) );
}

bool are_rectangles_concentric(rectangle_i a, rectangle_i b)
{
    return ( ( std::abs((a.origin.x+a.width/2) - (b.origin.x+b.width/2)) < 10 ) &&
             ( std::abs((a.origin.y+a.height/2) - (b.origin.y+b.height/2)) < 10 ) );
}

bool are_circles_overlapping(circle_i a, circle_i b)
{
    rectangle_i rect_a = cm::get_bounding_rect(a);
    rectangle_i rect_b = cm::get_bounding_rect(b);
    int overlapping_area = cm::overlapping_area(rect_a, rect_b);
    return(overlapping_area>(0.2*rect_b.height*rect_b.width));
}

bool are_rectangles_overlapping(rectangle_i rect_a, rectangle_i rect_b)
{
    int rect_a_area = rect_a.height*rect_a.width;
    int rect_b_area = rect_b.height*rect_b.width;
    int overlapping_area = cm::overlapping_area(rect_a, rect_b);
    if(rect_a_area>rect_b_area)
    {
        return(overlapping_area>(0.2*rect_b.height*rect_b.width));
    }
    else
    {
        return(overlapping_area>(0.2*rect_a.height*rect_a.width));
    }
}

}


// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/geometric_types.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/computed_result.cpp
// //////////////////////////////////////////////////////////////////////


cm::computed_objects::computed_objects()
{
    anchor_displacement.x = anchor_displacement.y = 0;
}

void cm::computed_objects::add_result(const cm::rectangle_i &rect_item)
{
    rectangles.push_back(rect_item);
}

void cm::computed_objects::add_keypoint(const cv::KeyPoint &keypoint_item)
{
    keypoints.push_back(keypoint_item);
}

void cm::computed_objects::add_descriptor(const cv::Mat &descriptor_item)
{
    descriptor = descriptor_item.clone();
//    descriptor.push_back(descriptor_item);
    CM_LOG <<"descriptor cols : "<<descriptor.cols<<", descriptor rows : "<<descriptor.rows<<std::endl;
}

void cm::computed_objects::add_corner(const cv::Point2f &corner)
{
    corners.push_back(corner);
}

void cm::computed_objects::set_displacement(cm::point_i displacement)
{
    anchor_displacement = displacement;
}

cm::point_i cm::computed_objects::get_displacement()
{
    return anchor_displacement;
}

void cm::computed_objects::displace_result(cm::point_i displacement)
{
    for( auto & rectangle : rectangles)
    {
        rectangle.origin.x += displacement.x;
        rectangle.origin.y += displacement.y;
    }
}

void cm::computed_objects::translate_result(const int pos_x, const int pos_y)
{
    for( auto & rectangle : rectangles)
    {
        rectangle.origin.x += pos_x;
        rectangle.origin.y += pos_y;
    }
    return;
}

void cm::computed_objects::clear()
{
    rectangles.clear();
    keypoints.clear();
    descriptor = cv::Mat();
    corners.clear();
    anchor_displacement.x = anchor_displacement.y = 0;
}

void cm::computed_objects::clear_models()
{
    dnn_obj.clear();
}

cm::computed_objects cm::computed_objects::operator +(const cm::computed_objects &obj)
{
    computed_objects result;
    result.rectangles = cm::add_vectors<rectangle_i>(rectangles, obj.rectangles);
    return result;
}

cm::rectangle_i cm::computed_objects::get_tracker_rect()
{
    return tracker_rectangle;
}

cm::vec_rectangle cm::computed_objects::get_rectangles()
{
    return rectangles;
}

std::vector<KeyPoint> cm::computed_objects::get_keypoints()
{
    return keypoints;
}

std::vector<Point2f> cm::computed_objects::get_corners()
{
    return corners;
}

cv::Mat cm::computed_objects::get_descriptor()
{
    return descriptor;
}

void cm::computed_objects::filter_concentric_shapes()
{
    filter_concentric_rectangles();
}

void cm::computed_objects::filter_overlapping_shapes()
{
    filter_overlapping_rectangles();
}

void cm::computed_objects::set_tracker_rectangle(const cm::rectangle_i &rect_result)
{
    tracker_rectangle = rect_result;
}

void cm::computed_objects::filter_concentric_rectangles()
{
    if(rectangles.size() == 0)
        return;

    std::vector<rectangle_i> filtered_rectangles;

    auto in_itr = rectangles.begin();
    filtered_rectangles.push_back(*in_itr);

    ++in_itr;
    while(in_itr != rectangles.end())
    {
        bool similar_rectangle_added = false;
        for(auto &f_rect : filtered_rectangles)
        {
            if( are_rectangles_concentric(*in_itr, f_rect) )
            {
                similar_rectangle_added = true;
                break;
            }
        }

        if(similar_rectangle_added == false)
        {
            filtered_rectangles.push_back(*in_itr);
        }

        ++in_itr;
    }

    rectangles = filtered_rectangles;
}

void cm::computed_objects::filter_overlapping_rectangles()
{
    if(rectangles.size() == 0)
        return;

    std::vector<rectangle_i> filtered_rectangles;

    auto in_itr = rectangles.begin();
    filtered_rectangles.push_back(*in_itr);

    ++in_itr;
    while(in_itr != rectangles.end())
    {
        bool similar_rectangle_added = false;
        for(auto &f_rect : filtered_rectangles)
        {
            if( are_rectangles_overlapping(*in_itr, f_rect) )
            {
                similar_rectangle_added = true;
                break;
            }
        }

        if(similar_rectangle_added == false)
        {
            filtered_rectangles.push_back(*in_itr);
        }

        ++in_itr;
    }

    rectangles = filtered_rectangles;
}

void cm::dnn_object::clear()
{
    dnn_results.clear();
}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/computed_result.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/json_reader.cpp
// //////////////////////////////////////////////////////////////////////


cm::block_param::block_param()
{
    block_name = "";
}

bool cm::algo_pipeline_params::load_file(const std::string &in_file)
{
    std::ifstream in_file_stream(in_file);
    try
    {
        in_file_stream >> root_object;
    }
    catch(...)
    {
        return false;
    }
    return true;
}

bool cm::algo_pipeline_params::load_buffer(const std::string &json_buffer)
{
    try
    {
        root_object = json::parse(json_buffer);
    }
    catch(...)
    {
        return false;
    }
    return true;
}

bool cm::algo_pipeline_params::setup_methods()
{
    algo_block_params.clear();
    //json block_object = root_object.at(0);

    for(auto itr1 = root_object.begin() ; itr1 != root_object.end() ; ++itr1)
    {
        json obj_itr1 = *itr1;
        for(auto itr2 = obj_itr1.begin() ; itr2 != obj_itr1.end() ; ++itr2)
        {
            json obj_itr2 = *itr2;
            for(auto itr3 = obj_itr2.begin() ; itr3 != obj_itr2.end() ; ++itr3)
            {
                json obj_itr3 = *itr3;
                block_param bp;
                bp.block_name = itr3.key();
                for(auto itr4 = obj_itr3.begin() ; itr4 != obj_itr3.end() ; ++itr4)
                {
                    bp.param_values.insert(std::pair<std::string, json>(itr4.key(), itr4.value()));
                }
                algo_block_params.push_back(bp);
            }
        }
    }

    return true;
}

bool cm::algo_pipeline_params::setup_methods(const std::string &block_name)
{
    algo_block_params.clear();
    json block_object = root_object[block_name];

    for(auto itr1 = block_object.begin() ; itr1 != block_object.end() ; ++itr1)
    {
        json obj_itr1 = *itr1;
        for(auto itr2 = obj_itr1.begin() ; itr2 != obj_itr1.end() ; ++itr2)
        {
            json obj_itr2 = *itr2;
            block_param bp;
            bp.block_name = itr2.key();
            for(auto itr3 = obj_itr2.begin() ; itr3 != obj_itr2.end() ; ++itr3)
            {
                bp.param_values.insert(std::pair<std::string, json>(itr3.key(), itr3.value()));
            }
            algo_block_params.push_back(bp);
        }
    }

    return true;
}

size_t cm::algo_pipeline_params::methods_size()
{
    return algo_block_params.size();
}

std::string cm::algo_pipeline_params::method_name(size_t index_num)
{
    std::string fn_name = "";

    if(index_num >= 0 && index_num < algo_block_params.size())
        fn_name = algo_block_params[index_num].block_name;

    return fn_name;
}

int cm::algo_pipeline_params::get_int_param(const std::string &block_name,
                                            const std::string &param_name,
                                            int default_param_value) const
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                return param_item->second.get<int>();
            }
        }
    }
    return default_param_value;
}

double cm::algo_pipeline_params::get_double_param(const std::string &block_name,
                                                  const std::string &param_name,
                                                  double default_param_value) const
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                return param_item->second.get<double>();
            }
        }
    }
    return default_param_value;
}

std::string cm::algo_pipeline_params::
get_string_param(const std::string &block_name,
                 const std::string &param_name,
                 std::string default_param_value) const
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                return param_item->second.get<std::string>();
            }
        }
    }
    return default_param_value;
}


std::vector<double> cm::algo_pipeline_params::get_vector_double_param(const std::string &block_name,
                                                                      const std::string &param_name,
                                                                      std::vector<double> default_param_value) const
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                return param_item->second.get<std::vector<double>>();
            }
        }
    }
    return default_param_value;
}

bool cm::algo_pipeline_params::get_bool_param(const string &block_name,
                                              const string &param_name,
                                              bool default_param_value) const
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                int bool_param_int_value = param_item->second.get<int>();
                if(bool_param_int_value == 1)
                    return true;
                else
                    return false;
            }
        }
    }
    return default_param_value;
}


bool cm::algo_pipeline_params::set_int_param(const std::string &block_name,
                                             const std::string &param_name,
                                             int param_value)
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                param_item->second = param_value;
                return true;
            }
        }
    }
    return false;
}

bool cm::algo_pipeline_params::set_double_param(const std::string &block_name,
                                                const std::string &param_name,
                                                double param_value)
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                param_item->second = param_value;
                return true;
            }
        }
    }
    return false;
}

bool cm::algo_pipeline_params::set_string_param(const std::string &block_name,
                                                const std::string &param_name,
                                                std::string param_value)
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                param_item->second = param_value;
                return true;
            }
        }
    }
    return false;
}

bool cm::algo_pipeline_params::set_bool_param(const string &block_name,
                                              const string &param_name,
                                              bool param_value)

{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                int bool_param_int_value = 0;
                if(param_value == true)
                    bool_param_int_value = 1;
                param_item->second = bool_param_int_value;
                return true;
            }
        }
    }
    return false;
}


bool cm::algo_pipeline_params::set_vector_double_param(const std::string &block_name,
                                                const std::string &param_name,
                                                std::vector<double> param_value)
{
    for(auto itr = algo_block_params.begin() ; itr != algo_block_params.end() ; ++itr)
    {
        if ( itr->block_name == block_name )
        {
            auto param_item = itr->param_values.find(param_name);
            if(param_item != itr->param_values.end())
            {
                param_item->second = param_value;
                return true;
            }
        }
    }
    return false;
}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/json_reader.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/image_holder.cpp
// //////////////////////////////////////////////////////////////////////


// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/image_holder.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/cv_image_holder.cpp
// //////////////////////////////////////////////////////////////////////


cm::cv_image_holder::cv_image_holder()
{
}

cm::cv_image_holder::cv_image_holder(const std::string & in_file)
{
}

cm::cv_image_holder::cv_image_holder(const cv::Mat & in_matrix, bool duplicate_locally)
{
    set_cv_matrix(in_matrix, duplicate_locally);
}

cm::cv_image_holder::~cv_image_holder()
{
}

int cm::cv_image_holder::get_buffer_height() const
{
    return 0;
}


int cm::cv_image_holder::get_buffer_width() const
{
    return 0;
}


int cm::cv_image_holder::get_height() const
{
    return ( cv_image.data != nullptr ) ? cv_image.cols : 0;
}


int cm::cv_image_holder::get_width() const
{
    return ( cv_image.data != nullptr ) ? cv_image.rows : 0;
}


const void * cm::cv_image_holder::get_pixels() const
{
    return ( cv_image.data != nullptr ) ?
                (static_cast < const void * > (cv_image.data)) :
                nullptr;

}

bool cm::cv_image_holder::is_valid() const
{
    return ( cv_image.data != nullptr ) ? true : false;
}

bool cm::cv_image_holder::write_file(const std::string &file_name) const
{
    return cv::imwrite(file_name.c_str(), cv_image);
}

cv::Mat cm::cv_image_holder::get_cv_matrix()
{
    return cv_image;
}

void cm::cv_image_holder::set_cv_matrix(const Mat &in_matrix, bool duplicate_locally)
{
    if(duplicate_locally)
    {
        cv_image = in_matrix.clone();
    }
    else
    {
        cv_image = in_matrix;
    }
}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/cv_image_holder.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/process_block.cpp
// //////////////////////////////////////////////////////////////////////


cm::process_block::process_block()
{
    input_image = output_image = nullptr;
}

cm::process_block::~process_block()
{
    if(input_image){
        delete input_image;
    }

    if(output_image){
        delete output_image;
    }
}

bool cm::process_block::set_input_image(cm::image_holder *in_image)
{
    input_image = dynamic_cast<cv_image_holder*>(in_image);

    return (input_image != nullptr);
}

bool cm::process_block::set_output_image(cm::image_holder *out_image)
{
    output_image = dynamic_cast<cv_image_holder*>(out_image);

    return (output_image != nullptr);
}

bool cm::process_block::set_process_block_parameters(const std::string & json_buffer)
{
    if(process_params.load_buffer(json_buffer) == false)
    {
        return false;
    }

    if(process_params.setup_methods() == false)
    {
        return false;
    }

    return true;
}

bool cm::process_block::set_process_block_parameters(const std::string & json_buffer,
                                                     const std::string & block_name)
{
    if(process_params.load_buffer(json_buffer) == false)
    {
        return false;
    }

    if(process_params.setup_methods(block_name) == false)
    {
        return false;
    }

    return true;
}

bool cm::process_block::set_results(computed_objects objs)
{
    objects_found = objs;
    return true;
}

cm::computed_objects cm::process_block::get_results()
{
    return objects_found;
}

bool cm::process_block::process_image()
{
    if(process_params.methods_size() == 0)
        return false;

    if(input_image == nullptr)
        return false;

    cv::Mat input_matrix = input_image->get_cv_matrix();
    cv::Mat output_matrix;

    if(output_image != nullptr)
    {
        output_matrix = output_image->get_cv_matrix();
    }

    if(input_matrix.data == nullptr)
        return false;

    if(output_matrix.data == nullptr)
        output_matrix = input_matrix;

    for(int itr = 0 ; itr < process_params.methods_size() ; itr++)
    {
        if(itr == 0)
        {
            prev_node_image = input_matrix;
            next_node_image = input_matrix;
        }

        if(itr == process_params.methods_size() - 1)
        {
            next_node_image = output_matrix;
        }

        auto method_name = process_params.method_name(itr);

        auto algo_fptr = cm::function_table::get_algo_block_function(method_name);

        if(algo_fptr != nullptr)
        {
            CM_LOG << "Running process block " << method_name << std::endl;

            algo_fptr(prev_node_image, next_node_image, process_params, objects_found);

            prev_node_image = next_node_image;
        }

        if(itr == process_params.methods_size() - 1)
        {
             output_image->set_cv_matrix(next_node_image);
        }
    }

    return true;
}

cm::image_holder *cm::process_block::get_output_image()
{
    return output_image;
}

bool cm::process_block::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      int value)
{
    return process_params.set_int_param(block_name, param_name, value);
}

bool cm::process_block::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      double value)
{
    return process_params.set_double_param(block_name, param_name, value);
}

bool cm::process_block::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      std::string value)
{
    return process_params.set_string_param(block_name, param_name, value);
}

bool cm::process_block::reset()
{
    objects_found.clear();

    return true;
}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/process_block.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/utilities.cpp
// //////////////////////////////////////////////////////////////////////



std::string cm::extract_function_name(std::string in_string)
{
    return in_string.substr(3);
}


cm::circle_i cm::get_circle(cv::Vec3f circle_vec)
{
    circle_i circle;
    circle.center.x = cvRound(circle_vec[0]);
    circle.center.y = cvRound(circle_vec[1]);
    circle.radius = cvRound(circle_vec[2]);
    return circle;
}


cm::point_i cm::get_cm_point(cv::Point in_pt)
{
    cm::point_i out_pt;
    out_pt.x = in_pt.x;
    out_pt.y = in_pt.y;
    return out_pt;
}


cv::Point cm::get_cv_point(cm::point_i in_pt)
{
    return cv::Point(in_pt.x, in_pt.y);
}

// Draw only the 2D points
void draw2DPoints(cv::Mat image, std::vector<cv::Point2f> &list_points, cv::Scalar color)
{
  for( size_t i = 0; i < list_points.size(); i++)
  {
    cv::Point2f point_2d = list_points[i];

    // Draw Selected points
    cv::circle(image, point_2d, 3, color, -1, 0 );
  }
}


std::vector<cm::point_i> cm::get_cm_contour(std::vector<cv::Point> contour_points)
{
    std::vector<cm::point_i> cm_contour;
    for(auto &point : contour_points)
    {
        cm_contour.push_back(get_cm_point(point));
    }
    return cm_contour;
}


std::vector<cv::Point> cm::get_cv_contour(std::vector<cm::point_i> contour_points)
{
    std::vector<cv::Point> cv_contour;
    for(auto &point : contour_points)
    {
        cv::Point pt = cm::get_cv_point(point);
        cv_contour.push_back(pt);
    }
    return cv_contour;
}

cv::Point cm::get_centroid(std::vector<cv::Point> contour)
{
    cv::Point centroid;
    cv::Moments rect_moments = cv::moments(contour);
    centroid.x = (int) ( rect_moments.m10 / rect_moments.m00);
    centroid.y = (int) ( rect_moments.m01 / rect_moments.m00);
    return centroid;
}

cm::rectangle_i cm::get_bounding_rect(cm::circle_i in)
{
    cm::rectangle_i out;

    out.origin.x = in.center.x - in.radius;
    out.origin.y = in.center.y - in.radius;
    out.width = out.height = in.radius * 2;

    return out;
}

double cm::get_length(line_i l)
{
    double dx1 = l.p1.x - l.p2.x;
    double dy1 = l.p1.y - l.p2.y;

    return std::sqrt( ( dx1 * dx1 ) + ( dy1 + dy1) );
}


cm::rectangle_i  cm::rect_2_bounding_rect(Rect2d v)
{
    cm::rectangle_i v_rectangle_i;

    v_rectangle_i.origin.x = v.x;
    v_rectangle_i.origin.y = v.y;
    v_rectangle_i.width = v.width;
    v_rectangle_i.height = v.height;

    return v_rectangle_i;
}

cm::rectangle_i cm::polygon_i_2_bounding_rect(polygon_i rect_contour)
{

    std::vector<cm::point_i> a = rect_contour.contour;
    vector<cv::Point2d> v1d=cm::vector_Point_i_2_vector_Point2d(a);
    vector<cv::Point> v1(v1d.begin(), v1d.end());
    Rect2d rect = boundingRect(v1);
    cm::rectangle_i  v = rect_2_bounding_rect(rect);

    return v;
}

cv::Rect cm::get_cv_rect(const cm::rectangle_i & cm_rect)
{
    cv::Rect cv_rect;
    cv_rect.x = cm_rect.origin.x;
    cv_rect.y = cm_rect.origin.y;
    cv_rect.width = cm_rect.width;
    cv_rect.height = cm_rect.height;

    return cv_rect;
}

vector<cm::rectangle_i> cm::vector_polygon_i_2_vector_bounding_rect(std::vector<polygon_i> found_rectangles)
{
    vector<cm::rectangle_i> rectangle_v;

    for(int rct = 0; rct<found_rectangles.size(); rct++){
        cm::rectangle_i  v = polygon_i_2_bounding_rect(found_rectangles[rct]);
        rectangle_v.push_back(v);
    }

    return rectangle_v;
}

vector<cm::rectangle_i> cm::get_vector_bounding_rect(vector<cm::circle_i> in)
{
    vector<cm::rectangle_i> out;
    cm::rectangle_i out1;

    for(auto & c : in){

        out1= cm::get_bounding_rect(c);
        out.push_back(out1);
    }
    return out;
}

cm::rectangle_i cm::bounding_rect_shifting(cm::rectangle_i v, int shift, int shift_value)
{
    cm::rectangle_i rc;

    /* 1 for "UP" , 2 for "DOWN" , 3 for "LEFT" , 4 for "RIGHT" ,*/
    const int str_up = 1;
    const int str_down = 2;
    const int str_left = 3;
    const int str_right = 4;

    switch(shift) {
    case str_up  :
        rc.origin.x = v.origin.x;
        rc.origin.y = v.origin.y-shift_value;
        rc.width = v.width;
        rc.height = v.height;
        CM_LOG<<"   UP shift"<<std::endl;
        break; //optional
    case str_down  :
        rc.origin.x = v.origin.x;
        rc.origin.y = v.origin.y+shift_value;
        rc.width = v.width;
        rc.height = v.height;
        CM_LOG<<"   DOWN shift"<<std::endl;
        break; //optional
    case str_left  :
        rc.origin.x = v.origin.x-shift_value;
        rc.origin.y = v.origin.y;
        rc.width = v.width;
        rc.height = v.height;
        CM_LOG<<"   LEFT shift"<<std::endl;
        break; //optional
    case str_right  :
        rc.origin.x = v.origin.x+shift_value;
        rc.origin.y = v.origin.y;
        rc.width = v.width;
        rc.height = v.height;
        CM_LOG<<"   RIGTH shift"<<std::endl;
        break; //optional
    default :
        CM_LOG<<"   shift is not posible"<<std::endl;
    }
    return rc;
}

cm::polygon_i cm::vector_point_2_polygon_i(vector<cv::Point> vd, double shape_appoximator_ratio)
{
    cm::polygon_i rect_contour;
    double contour_perimeter = arcLength(vd, true);
    vector<cv::Point> approx_shape;
    approxPolyDP(vd,
                 approx_shape,
                 shape_appoximator_ratio * contour_perimeter,
                 true);
    rect_contour.perimeter = contour_perimeter;
    rect_contour.contour = cm::get_cm_contour(approx_shape);
    rect_contour.centroid = cm::get_cm_point(cm::get_centroid(approx_shape));
    return rect_contour;
}

vector<Point2d> cm::vector_Point_i_2_vector_Point2d(std::vector<cm::point_i> a){

    vector<Point2d> vd;

    vd = { {double(a[0].x), double(a[0].y)},
           {double(a[1].x), double(a[1].y)},
           {double(a[2].x), double(a[2].y)},
           {double(a[3].x), double(a[3].y)} };
    return vd;
}

std::vector<cv::Point> cm::bounding_rect_2_vector_point(cm::rectangle_i rc){

    std::vector<cv::Point> vd;

    vd = { { int(rc.origin.x), int(rc.origin.y) },
           { int(rc.origin.x+rc.width), int(rc.origin.y)},
           { int(rc.origin.x+rc.width), int(rc.origin.y+rc.height)},
           { int(rc.origin.x), int(rc.origin.y+rc.height) } };
    return vd;
}

std::pair<int, int> cm::find_histogram(int bins, vector<cm::rectangle_i> check)
{
    std::pair<int, int> bin_threshold;
    Mat group_radius_;
    vector<float> group_radius;

    CM_LOG<<"check size "<<check.size()<<std::endl;
    for(cm::rectangle_i & c : check)
    {
        group_radius_.push_back(float(c.width*c.height));
        group_radius.push_back(float(c.width*c.height));
    }
    CM_LOG<<group_radius.size()<<"group_radius.size()"<<group_radius_.size()<<std::endl;


    //min and max area
    float min_value = *std::min_element(group_radius.begin(),group_radius.end());
    float max_value = *std::max_element(group_radius.begin(),group_radius.end());
    CM_LOG<<"min area "<<min_value <<" max area "<<max_value<<std::endl;

    int bin_size = int((max_value - min_value)/bins);
    CM_LOG<<"bin size :"<<bin_size<<std::endl;

    int histSize[] = {bins};

    // Set ranges for histogram bins
    float lranges[] = {min_value, max_value};
    const float* ranges[] = {lranges};

    // create matrix for histogram
    cv::Mat hist;
    int channels[] = {0};

    //calculate hist
    cv::calcHist(&group_radius_, 1, channels, cv::Mat(), hist, 1, histSize, ranges, true, false);

    CM_LOG<<hist<<std::endl;

    double min_val, max_val;
    cv::minMaxLoc(hist, &min_val, &max_val);
    CM_LOG<<"max_hist_value "<<max_val<<std::endl;


    CM_LOG<<hist.size()<<std::endl;

    int minArea, maxArea;
    for (int j=0; j < hist.rows; j++){
        if(max_val==hist.at<float>(0,j)){

            minArea =  min_value+(j*bin_size);
            maxArea =  minArea+bin_size;
        }
    }

    bin_threshold = std::make_pair(minArea, maxArea);

    return bin_threshold;
}

vector<std::pair<int, int>> cm::find_histogram_based_area_groups(int bins, vector<cm::rectangle_i> check)
{
    vector<std::pair<int, int>> bins_threshold;
    Mat group_radius_;
    vector<float> group_radius;

    CM_LOG<<"check size "<<check.size()<<std::endl;
    for(cm::rectangle_i & c : check)
    {
        group_radius_.push_back(float(c.width*c.height));
        group_radius.push_back(float(c.width*c.height));
    }
    CM_LOG<<group_radius.size()<<"group_radius.size()"<<group_radius_.size()<<std::endl;


    //min and max area
    float min_value = *std::min_element(group_radius.begin(),group_radius.end());
    float max_value = *std::max_element(group_radius.begin(),group_radius.end());
    CM_LOG<<"min area "<<min_value <<" max area "<<max_value<<std::endl;

    int bin_size = int((max_value - min_value)/bins);
    CM_LOG<<"bin size :"<<bin_size<<std::endl;

    int histSize[] = {bins};

    // Set ranges for histogram bins
    float lranges[] = {min_value, max_value};
    const float* ranges[] = {lranges};

    // create matrix for histogram
    cv::Mat hist;
    int channels[] = {0};

    cv::calcHist(&group_radius_, 1, channels, cv::Mat(), hist, 1, histSize, ranges, true, false);

    CM_LOG<<hist<<std::endl;

    double min_val, max_val;
    cv::minMaxLoc(hist, &min_val, &max_val);
    CM_LOG<<"max_hist_value "<<max_val<<std::endl;

    CM_LOG<<hist.size()<<std::endl;

    int minArea, maxArea;
    for (int j=0; j < hist.rows; j++){

        minArea =  min_value+(j*bin_size);
        maxArea =  minArea+bin_size;

        std::pair<int, int> bin_threshold;
        bin_threshold=std::make_pair(minArea, maxArea);

        bins_threshold.push_back(bin_threshold);


    }

    CM_LOG<<"Number of bins : "<<bins_threshold.size()<<std::endl;



    return bins_threshold;
}

int cm::overlapping_area(cm::rectangle_i v, cm::rectangle_i vv){

    int xa1, ya1, xa2, ya2;
    int xb1, yb1, xb2, yb2;

    xa1 = v.origin.x;
    ya1 = v.origin.y;
    xa2 = v.origin.x+v.width;
    ya2 =v.origin.y+v.height;

    xb1 = vv.origin.x;
    yb1 = vv.origin.y;
    xb2 = vv.origin.x+vv.width;
    yb2 =vv.origin.y+vv.height;

    int iLeft = max(xa1, xb1);
    int iRight = min(xa2, xb2);
    int iTop = max(ya1, yb1);
    int iBottom = min(ya2, yb2);

    int area =max(0, iRight - iLeft) * max(0, iBottom - iTop);

    return area;

}

float cm::get_eucliedean_distance(Point2f a, Point2f b)
{
    float sq_dist = std::pow( ( a.x - b.x ) , 2.0) + std::pow( ( a.y - b.y ) , 2.0);
    return std::sqrt(sq_dist);
}


double cm::otsu_threshold(Mat in)
{
    Mat k;
    double otsu_thresh;

    if(in.type()==0)
    {
        otsu_thresh = cv::threshold(in, k, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU);
        CM_LOG<<"otsu threshold : "<<otsu_thresh<<std::endl;
    }
    else
    {
        CM_LOG<<"image type is not CV_8U "<<std::endl;
        otsu_thresh = 0.0;
    }

    return otsu_thresh;
}

float cm::distance_btw_point_i(cm::point_i p, cm::point_i q)
{
    Point diff;
    diff.x= abs(p.x - q.x);
    diff.y= abs(p.y - q.y);
    return cv::sqrt(diff.x*diff.x + diff.y*diff.y);
}



Mat cm::translate_image(Mat img, int offset_x, int offset_y)
{
    Mat dest = img.clone();
    Mat trans_mat = (Mat_<double>(2,3) << 1, 0, offset_x, 0, 1, offset_y);
    cv::warpAffine(img, dest, trans_mat, img.size());
    return dest;
}

Mat cm::rotate_image(Mat img, int rotation_angle)
{
    Mat dest = img.clone();
    Mat rotn_mat = cv::getRotationMatrix2D(cv::Point2f(img.rows / 2, img.cols / 2) ,
                                           rotation_angle, 1);
    cv::warpAffine(img, dest, rotn_mat, img.size());
    return dest;
}

Mat cm::scale_image(Mat img, int increase_percentage)
{
    Mat dest = img.clone();
    double dt = ( 100 + increase_percentage ) / 100.0;
    int resize_type = INTER_LINEAR;
    if(increase_percentage < 0)
        resize_type = INTER_AREA;
    cv::resize(img, dest, Size(), dt, dt, resize_type);
    return dest;
}

vector<Mat> cm::get_geometric_tranforms(Mat in)
{
    vector<Mat> images;
    images.push_back(in);

    images.push_back(rotate_image(in, 3));
    images.push_back(rotate_image(in, 7));
    images.push_back(rotate_image(in, -3));
    images.push_back(rotate_image(in, -7));

    images.push_back(scale_image(in, 5));
    images.push_back(scale_image(in, -5));

    return images;
}


// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/utilities.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/filters.cpp
// //////////////////////////////////////////////////////////////////////



namespace cm {

namespace libopencv {

bool difference_of_gaussian( Mat & in, Mat & out,
                             const algo_pipeline_params & params,
                             computed_objects & result_obj )
{
    cv::Size ksize_1(9,9), ksize_2(3,3);
    double sigmaX = 2.0;
    double sigmaY = 2.0;
    int  k_min_threshold = 127;
    int  k_max_threshold = 255;

    (void)(result_obj);

    try
    {
        Mat out1, out2;
        read_int_param("difference_of_gaussian", ksize_1.width);
        read_int_param("difference_of_gaussian", ksize_1.height);
        read_int_param("difference_of_gaussian", ksize_2.width);
        read_int_param("difference_of_gaussian", ksize_2.height);
        read_double_param("difference_of_gaussian", sigmaX);
        read_double_param("difference_of_gaussian", sigmaY);
        read_double_param("difference_of_gaussian", k_min_threshold);
        read_double_param("difference_of_gaussian", k_max_threshold);

        cv::GaussianBlur(in, out1, ksize_1, sigmaX, sigmaY);
        cv::GaussianBlur(in, out2, ksize_2, sigmaX, sigmaY);

        out = out2 - out1;

        cv::cvtColor( out, out, CV_BGR2GRAY );
        cv::equalizeHist( out, out );
        cv::threshold(out, out, k_min_threshold, k_max_threshold, THRESH_BINARY);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool gaussian_blur( Mat & in, Mat & out,
                    const algo_pipeline_params & params,
                    computed_objects & result_obj )
{
    cv::Size ksize(5,5);
    double sigmaX = 2.0;
    double sigmaY = 2.0;

    (void)(result_obj);

    try
    {
        read_int_param("gaussian_blur", ksize.width);
        read_int_param("gaussian_blur", ksize.height);
        read_double_param("gaussian_blur", sigmaX);
        read_double_param("gaussian_blur", sigmaY);

        cv::GaussianBlur(in, out, ksize, sigmaX, sigmaY);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool median_blur( Mat & in, Mat & out,
                  const algo_pipeline_params & params,
                  computed_objects & result_obj )
{
    int ksize = 5;

    (void)(result_obj);

    try
    {
        read_int_param("median_blur", ksize);

        if(ksize %2==0){
            ksize = ksize+1;
        }

        cv::medianBlur(in, out, ksize);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool bilateral_filter( Mat & in, Mat & out,
                       const algo_pipeline_params & params,
                       computed_objects & result_obj )
{
    int d = 9;
    double sigmaColor = 75.0;
    double sigmaSpace = 75.0;

    (void)(result_obj);

    try
    {
        read_int_param("bilateral_filter", d);
        read_double_param("bilateral_filter", sigmaColor);
        read_double_param("bilateral_filter", sigmaSpace);

        cv::bilateralFilter(in, out, d, sigmaColor, sigmaSpace);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool convert_color_to_gray( Mat & in, Mat & out,
                            const algo_pipeline_params & params,
                            computed_objects & result_obj )
{
    (void)(params);
    (void)(result_obj);

    try
    {
        cv::cvtColor(in, out, CV_BGR2GRAY);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool canny_edges( Mat & in, Mat & out,
                  const algo_pipeline_params & params,
                  computed_objects & result_obj )
{
    double kernel_size = 3.0;
    double min_threshold = 75.0;
    double max_threshold = 150.0;

    (void)(result_obj);

    try
    {
        read_double_param("canny_edges", kernel_size);
        read_double_param("canny_edges", min_threshold);
        read_double_param("canny_edges", max_threshold);

        cv::Canny(in, out, min_threshold, max_threshold, kernel_size);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool otsu_binarization( Mat & in, Mat & out,
                        const algo_pipeline_params & params,
                        computed_objects & result_obj )
{
    double min_threshold = 0.0;
    double max_threshold = 100.0;

    (void)(result_obj);

    try
    {
        read_double_param("otsu_binarization", min_threshold);
        read_double_param("otsu_binarization", max_threshold);

        cv::threshold(out, out, min_threshold, max_threshold, THRESH_BINARY | THRESH_OTSU);
    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool box_filter_blur( Mat & in, Mat & out,
                      const algo_pipeline_params & params,
                      computed_objects & result_obj )
{
    cv::Size ksize(3,3);

    (void)(result_obj);

    try
    {
        read_int_param("box_filter_blur", ksize.width);
        read_int_param("box_filter_blur", ksize.height);

        cv::blur(in, out, ksize);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool laplacian_operator( Mat & in, Mat & out,
                         const algo_pipeline_params & params,
                         computed_objects & result_obj )
{
    int ddepth = 1;
    int ksize = 1;
    double scale = 1.0;
    double delta = 0.0;

    (void)(result_obj);

    try
    {
        read_int_param("laplacian_operator", ddepth);
        read_int_param("laplacian_operator", ksize);
        read_double_param("laplacian_operator", scale);
        read_double_param("laplacian_operator", delta);

        Mat dst;
        cv::Laplacian(in, dst, ddepth, ksize, scale, delta);
        cv::convertScaleAbs(dst, out);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool sobel_operator( Mat & in, Mat & out,
                     const algo_pipeline_params & params,
                     computed_objects & result_obj )
{
    int ddepth = 1;
    int ksize = 1;
    int dx = 0;
    int dy = 0;
    double scale = 1.0;
    double delta = 0.0;

    (void)(result_obj);

    try
    {
        read_int_param("sobel_operator", ddepth);
        read_int_param("sobel_operator", dx);
        read_int_param("sobel_operator", dy);
        read_int_param("sobel_operator", ksize);
        read_double_param("sobel_operator", scale);
        read_double_param("sobel_operator", delta);

        Mat grad_x, grad_y;
        Mat abs_grad_x, abs_grad_y;

        cv::Sobel(in, grad_x, ddepth, dx, 0, ksize, scale, delta, cv::BORDER_DEFAULT);
        cv::convertScaleAbs(grad_x, abs_grad_x);

        cv::Sobel(in, grad_y, ddepth, 0, dy, ksize, scale, delta, cv::BORDER_DEFAULT);
        cv::convertScaleAbs(grad_y, abs_grad_y);

        cv::addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, out);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/filters.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/geometric_transform.cpp
// //////////////////////////////////////////////////////////////////////




namespace cm {

namespace libopencv {

bool resize( Mat & in, Mat & out,
             const algo_pipeline_params & params,
             computed_objects & result_obj )
{
    cv::Size dsize(640, 480);
    (void)(result_obj);
    try
    {
        read_int_param("resize", dsize.width);
        read_int_param("resize", dsize.height);

        cv::resize(in, out, dsize);

    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool set_region_of_interest( Mat & in, Mat & out,
                             const algo_pipeline_params & params,
                             computed_objects & result_obj )
{
    int pos_x = 0;
    int pos_y = 0;
    int width = 0;
    int height = 0;
    (void)(result_obj);
    try
    {
        read_int_param("set_region_of_interest", pos_x);
        read_int_param("set_region_of_interest", pos_y);
        read_int_param("set_region_of_interest", width);
        read_int_param("set_region_of_interest", height);

        if(width == 0 || height == 0)
            return true;

        in = cv::Mat(in, cv::Rect(pos_x, pos_y, width, height));
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool translate_result( Mat & in, Mat & out,
                       const algo_pipeline_params & params,
                       computed_objects & result_obj )
{
    int pos_x = 0;
    int pos_y = 0;
    (void)(result_obj);

    try
    {
        read_int_param("translate_result", pos_x);
        read_int_param("translate_result", pos_y);

        if(pos_x == 0 && pos_y == 0)
            return true;

        result_obj.translate_result(pos_x, pos_y);

    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool geometric_filtering( Mat & in, Mat & out,
                          const algo_pipeline_params & params,
                          computed_objects & result_obj )
{
    try
    {
        result_obj.filter_concentric_shapes();
        result_obj.filter_overlapping_shapes();
    }
    catch(...)
    {
        return false;
    }


    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/geometric_transform.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/shape_finder_impl.cpp
// //////////////////////////////////////////////////////////////////////



namespace cm {

namespace libopencv {

bool circle_finder_impl(Mat& in,
                        double dp,
                        double min_dist,
                        double canny_param_1,
                        double canny_param_2,
                        int min_radius,
                        int max_radius,
                        bool clear_and_add,
                        computed_objects & objects_found)
{

    try
    {
        std::vector<cv::Vec3f> circles;
        cv::HoughCircles(in, circles, CV_HOUGH_GRADIENT,
                         dp, min_dist, canny_param_1, canny_param_2,
                         min_radius, max_radius);

        if(clear_and_add == true)
        {
            objects_found.clear();
        }

        for(auto &c : circles)
        {
            auto cm_circle = cm::get_circle(c);
            auto cm_circle_bbox = cm::get_bounding_rect(cm_circle);
            objects_found.add_result(cm_circle_bbox);
        }

    }
    catch(...)
    {
        return false;
    }

    return true;
}

double angle_between_points(cv::Point pt1, cv::Point pt2, cv::Point pt0)
{
    double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

bool rectangle_finder_impl(Mat &in,
                           int threshold_step,
                           double shape_appoximator_ratio,
                           double min_contour_area,
                           double max_contour_area,
                           double cosine_angle,
                           bool clear_and_add,
                           computed_objects &rectangles_found)
{
    try
    {
        if(clear_and_add == true)
        {
            rectangles_found.clear();
        }


        int image_area = in.rows * in.cols;
        int min_rect_area = image_area * min_contour_area;
        int max_rect_area = image_area * max_contour_area;

        for( int threshold_itr = k_threshold_min;
             threshold_itr < k_threshold_max ;
             threshold_itr += threshold_step)
        {
            Mat gray_thres;
            threshold(in, gray_thres, threshold_itr, k_threshold_max, THRESH_BINARY);

            vector<vector<cv::Point> > contours;
            findContours(gray_thres, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);

            for(int contour_index = 0;
                contour_index != contours.size() ;
                contour_index++)
            {
                double contour_perimeter = arcLength(contours[contour_index], true);

                vector<cv::Point> approx_shape;
                approxPolyDP(contours[contour_index],
                             approx_shape,
                             shape_appoximator_ratio * contour_perimeter,
                             true);

                int shape_size = approx_shape.size();
                if (shape_size != 4)
                    continue;

                double contour_area = contourArea(approx_shape);
                if ( contour_area < min_rect_area || contour_area > max_rect_area )
                    continue;

                vector<double> cosine_angles;
                for (int j = 2; j < shape_size + 1; j++)
                {
                    double cos_angle = angle_between_points (
                                approx_shape [j%shape_size],
                            approx_shape[j-2],
                            approx_shape[j-1]);
                    cosine_angles.push_back(cos_angle);
                }

                sort(cosine_angles.begin(), cosine_angles.end());
                double max_cosine_angle = cosine_angles.back();
                if(max_cosine_angle < cosine_angle)
                {
                    polygon_i rect_contour;
                    rect_contour.perimeter = contour_perimeter;
                    rect_contour.contour = cm::get_cm_contour(approx_shape);
                    rect_contour.centroid = cm::get_cm_point(cm::get_centroid(approx_shape));

                    rectangle_i rct = cm::polygon_i_2_bounding_rect(rect_contour);
                    CM_LOG << "Rectangle = " <<
                                 "( " << rct.origin.x << "," <<
                                 rct.origin.y << "," <<
                                 rct.width << "," <<
                                 rct.height << " )"<< std::endl;

                    rectangles_found.add_result(rct);
                }
            }
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool line_finder_impl(Mat &in,
                      double rho,
                      double theta,
                      double threshold,
                      double length_factor,
                      double max_seperation,
                      bool clear_and_add,
                      computed_objects &lines_found)
{
    try
    {
        if(clear_and_add == true)
        {
            lines_found.clear();
        }


        double avg_image_dimension = (in.rows + in.cols) / 2.0;

        std::vector<cv::Vec4i> lines;

        HoughLinesP(in, lines, rho, theta,
                    threshold,
                    avg_image_dimension * length_factor,
                    max_seperation);

//        for( size_t i = 0; i < lines.size(); i++ )
//        {
//            line_i line_item;
//            line_item.p1.x = lines[i][0];
//            line_item.p1.y = lines[i][1];
//            line_item.p2.x = lines[i][2];
//            line_item.p2.y = lines[i][3];

//            lines_found.add_result(line_item);
//        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool get_tracker(Mat & image1, Mat & image2, cm::rectangle_i &tracker, int min_good_features, int nfeatures)
{
    try
    {
        tracker.origin.x = 0;
        tracker.origin.y = 0;
        tracker.width = 0;
        tracker.height = 0;
        cv::Ptr<cv::FeatureDetector> ORB = cv::ORB::create(nfeatures);
        std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
        ORB->detect( image1, keypoints_object );
        ORB->detect( image2, keypoints_scene );
        cv::Mat descriptors_object, descriptors_scene;
        ORB->compute( image1, keypoints_object, descriptors_object );
        ORB->compute( image2, keypoints_scene, descriptors_scene );
        cv::BFMatcher matcher(cv::NORM_HAMMING, true);
        std::vector< cv::DMatch > good_matches;
        matcher.match( descriptors_object, descriptors_scene, good_matches );
        std::vector<cv::Point2f> obj1;
        std::vector<cv::Point2f> obj2;
        if(good_matches.size() >= min_good_features)
        {
            for( int i = 0; i < int(good_matches.size()); i++ )
            {
                obj1.push_back( keypoints_object[ good_matches[i].queryIdx ].pt );
                obj2.push_back( keypoints_scene[ good_matches[i].trainIdx ].pt );
            }
            if ( obj1.size() != 0 && obj2.size() != 0 )
            {
                cv::Mat H = cv::findHomography( obj1, obj2, CV_RANSAC );
                std::vector<cv::Point2f> obj_corners(4);
                obj_corners[0] = cvPoint(0,0);
                obj_corners[1] = cvPoint( image2.cols, 0 );
                obj_corners[2] = cvPoint( image2.cols, image2.rows );
                obj_corners[3] = cvPoint( 0, image2.rows );
                std::vector<cv::Point2f> scene_corners(4);
                cv::perspectiveTransform( obj_corners, scene_corners, H);
                cv::Mat img_matches;
                cv::warpPerspective(image1,
                                    img_matches,
                                    H,
                                    cv::Size(image2.cols,
                                             image2.rows));
                vector<Point2f> dstPoints, srcPoints;
                dstPoints.push_back(Point2f(0,0));
                dstPoints.push_back(Point2f(0,float(image2.rows)));
                dstPoints.push_back(Point2f(float(image2.cols),float(image2.rows)));
                dstPoints.push_back(Point2f(float(image2.cols),0));
                cv::perspectiveTransform(dstPoints,srcPoints,H.inv());
                RotatedRect minRect = minAreaRect(srcPoints);
                cv::Rect rect  = minRect.boundingRect();
                tracker.origin.x = rect.x;
                tracker.origin.y = rect.y;
                tracker.width = rect.width;
                tracker.height = rect.height;
                Rect RectangleToDraw3(tracker.origin.x,tracker.origin.y,tracker.width,tracker.height);
                cv::rectangle(image1, RectangleToDraw3, Scalar(0, 250, 0), 2, 8, 0);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    catch(...)
    {
        return false;
    }
}

void drawBoundingBox(Mat image, vector<Point2f> bb)
{
    for(unsigned i = 0; i < bb.size() - 1; i++) {
        cv::line(image, bb[i], bb[i + 1], Scalar(0, 0, 255), 2);
    }
    cv::line(image, bb[bb.size() - 1], bb[0], Scalar(0, 0, 255), 2);
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/shape_finder_impl.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/shape_finder.cpp
// //////////////////////////////////////////////////////////////////////



namespace cm {

namespace libopencv {

bool circle_finder( Mat & in, Mat & out,
                    const algo_pipeline_params & params,
                    computed_objects & result_obj )
{
    double dp = 2.0;
    double minDist = 0.0;
    double cannyParam1 = 100.0;
    double cannyParam2 = 100.0;
    int minRadius = 0;
    int maxRadius = 0;
    bool iterativeFinder = false;

    try
    {
        read_double_param("circle_finder", dp);
        read_double_param("circle_finder", minDist);
        read_double_param("circle_finder", cannyParam1);
        read_double_param("circle_finder", cannyParam2);
        read_int_param("circle_finder", minRadius);
        read_int_param("circle_finder", maxRadius);
        read_bool_param("circle_finder", iterativeFinder);

        if(iterativeFinder == true)
        {
            vector<Mat> images = cm::get_geometric_tranforms(in);

            for(auto &img : images)
            {
                circle_finder_impl(img, dp, minDist, cannyParam1, cannyParam2,
                                   minRadius, maxRadius, false, result_obj);
            }
        }
        else
        {
            circle_finder_impl(in, dp, minDist, cannyParam1, cannyParam2,
                               minRadius, maxRadius, true, result_obj);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool rectangle_finder( Mat & in, Mat & out,
                       const algo_pipeline_params & params,
                       computed_objects & result_obj )
{
    int threshold_step = 45;
    double shape_appoximator_ratio = 0.03;
    double min_contour_area = 0.001;
    double max_contour_area = 0.25;
    double cosine_angle = 0.4;
    bool iterativeFinder = false;

    try
    {
        read_int_param("rectangle_finder", threshold_step);
        read_double_param("rectangle_finder", shape_appoximator_ratio);
        read_double_param("rectangle_finder", min_contour_area);
        read_double_param("rectangle_finder", max_contour_area);
        read_double_param("rectangle_finder", cosine_angle);
        read_bool_param("rectangle_finder", iterativeFinder);

        if(iterativeFinder == true)
        {
            vector<Mat> images = cm::get_geometric_tranforms(in);

            for(auto &img : images)
            {
                rectangle_finder_impl(in, threshold_step, shape_appoximator_ratio,
                                      min_contour_area, max_contour_area,
                                      cosine_angle, false, result_obj);
            }
        }
        else
        {
            rectangle_finder_impl(in, threshold_step, shape_appoximator_ratio,
                                  min_contour_area, max_contour_area,
                                  cosine_angle, true, result_obj);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/shape_finder.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/ml_finder.cpp
// //////////////////////////////////////////////////////////////////////




namespace cm {

namespace libopencv {

bool yolo_model_loader( Mat & in, Mat & out,
                        const algo_pipeline_params & params,
                        computed_objects & result_obj )

{
    try
    {

        result_obj.clear_models();
        result_obj.clear();

        string model_configuration_file , model_weights_file;
        string class_names_file;

        read_string_param("yolo_model_loader", model_configuration_file);
        read_string_param("yolo_model_loader", model_weights_file);
        read_string_param("yolo_model_loader", class_names_file);

        cv::String model_configuration
                = cv::String(model_configuration_file.c_str());
        cv::String model_weights
                = cv::String(model_weights_file.c_str());

        result_obj.dnn_obj.dnn_net = cv::dnn::readNetFromDarknet(model_configuration,
                                                                 model_weights);
        if(result_obj.dnn_obj.dnn_net.empty())
            return false;
        else
        {
            ifstream class_names_file_obj(class_names_file);
            if (class_names_file_obj.is_open())
            {
                string name_item = "";
                while (std::getline(class_names_file_obj, name_item))
                    result_obj.dnn_obj.dnn_class_names.push_back(name_item);
            }
            class_names_file_obj.close();

            return true;
        }
    }
    catch(...)
    {
        return false;
    }
    return true;
}

bool yolo_object_finder( Mat & in, Mat & out,
                         const algo_pipeline_params & params,
                         computed_objects & result_obj )

{
    try
    {
        result_obj.clear();

        int in_width = 416;
        int in_height = 416;
        double confidence_threshold = 0.75;

        read_int_param("yolo_object_finder", in_width);
        read_int_param("yolo_object_finder", in_height);
        read_double_param("yolo_object_finder", confidence_threshold);

        Mat inputBlob = cv::dnn::blobFromImage(in,
                                               1 / 255.F,
                                               Size(in_width, in_height),
                                               Scalar(), true, false);

        result_obj.dnn_obj.dnn_net.setInput(inputBlob, "data");

        Mat detectionMat = result_obj.dnn_obj.dnn_net.forward("detection_out");

        for (int i = 0; i < detectionMat.rows; i++)
        {
            const int probability_index = 5;
            const int probability_size = detectionMat.cols - probability_index;
            float *prob_array_ptr = &detectionMat.at<float>(i, probability_index);

            size_t obj_class_index = max_element(prob_array_ptr,
                                                 prob_array_ptr + probability_size)
                    - prob_array_ptr;
            float confidence = detectionMat.at<float>
                    (i, (int)obj_class_index + probability_index);

            if (confidence > confidence_threshold)
            {
                float x_center = detectionMat.at<float>(i, 0) * in.cols;
                float y_center = detectionMat.at<float>(i, 1) * in.rows;
                float width = detectionMat.at<float>(i, 2) * in.cols;
                float height = detectionMat.at<float>(i, 3) * in.rows;
                Point p1(cvRound(x_center - width / 2),
                         cvRound(y_center - height / 2));
                Point p2(cvRound(x_center + width / 2),
                         cvRound(y_center + height / 2));

                dnn_result dnn_result_item;
                dnn_result_item.object_rectangle = cm::rect_2_bounding_rect(
                            Rect(p1, p2));

                if(obj_class_index > 0 &&
                        obj_class_index < result_obj.dnn_obj.dnn_class_names.size())
                {
                    dnn_result_item.object_label =
                            result_obj.dnn_obj.dnn_class_names[obj_class_index];
                }

                result_obj.dnn_obj.dnn_results.push_back(dnn_result_item);
            }
        }
    }
    catch(...)
    {
        return false;
    }
    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/ml_finder.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/feature_extraction.cpp
// //////////////////////////////////////////////////////////////////////




namespace cm {

namespace libopencv {

bool sift_feature_detection( Mat & in, Mat & out,
                             const algo_pipeline_params & params,
                             computed_objects & result_obj )
{
    int nfeatures = 0;
    int nOctaveLayers = 3;
    double contrastThreshold = 0.04;
    double edgeThreshold = 10;
    double sigma = 1.6;

    try
    {
        read_int_param("sift_feature_detection", nfeatures);
        read_int_param("sift_feature_detection", nOctaveLayers);
        read_double_param("sift_feature_detection", contrastThreshold);
        read_double_param("sift_feature_detection", edgeThreshold);
        read_double_param("sift_feature_detection", sigma);

        cv::Ptr<cv::Feature2D> detector = cv::xfeatures2d::SIFT::create(nfeatures,
                                                                        nOctaveLayers,
                                                                        contrastThreshold,
                                                                        edgeThreshold,
                                                                        sigma);

        std::vector<KeyPoint> keypoints_1;

        detector->detect( in, keypoints_1 );

        for(auto keypoint : keypoints_1){
            result_obj.add_keypoint(keypoint);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool sift_descriptor_extraction( Mat & in, Mat & out,
                                 const algo_pipeline_params & params,
                                 computed_objects & result_obj )
{
    int nfeatures = 0;
    int nOctaveLayers = 3;
    double contrastThreshold = 0.04;
    double edgeThreshold = 10;
    double sigma = 1.6;

    try
    {
        read_int_param("sift_descriptor_extraction", nfeatures);
        read_int_param("sift_descriptor_extraction", nOctaveLayers);
        read_double_param("sift_descriptor_extraction", contrastThreshold);
        read_double_param("sift_descriptor_extraction", edgeThreshold);
        read_double_param("sift_descriptor_extraction", sigma);

        auto keypoints_1 = result_obj.get_keypoints();

        cv::Ptr<cv::Feature2D> extractor = cv::xfeatures2d::SIFT::create(nfeatures,
                                                                         nOctaveLayers,
                                                                         contrastThreshold,
                                                                         edgeThreshold,
                                                                         sigma);


        cv::Mat descriptors_object;
        extractor->compute( in, keypoints_1, descriptors_object );

        result_obj.add_descriptor(descriptors_object);
    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool surf_feature_detection( Mat & in, Mat & out,
                             const algo_pipeline_params & params,
                             computed_objects & result_obj )
{
    int nOctaves = 4;
    int nOctaveLayers = 3;
    double hessianThreshold=100;
    bool extended = false;
    bool upright = false;

    try
    {
        read_int_param("surf_feature_detection", nOctaves);
        read_int_param("surf_feature_detection", nOctaveLayers);
        read_double_param("surf_feature_detection", hessianThreshold);
        read_bool_param("surf_feature_detection", extended);
        read_bool_param("surf_feature_detection", upright);

        cv::Ptr<cv::Feature2D> detector = cv::xfeatures2d::SURF::create(hessianThreshold,
                                                                        nOctaves,
                                                                        nOctaveLayers,
                                                                        extended,
                                                                        upright);

        std::vector<KeyPoint> keypoints_1;

        detector->detect( in, keypoints_1 );

        for(auto keypoint : keypoints_1){

            result_obj.add_keypoint(keypoint);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool surf_descriptor_extraction( Mat & in, Mat & out,
                                 const algo_pipeline_params & params,
                                 computed_objects & result_obj )
{
    int nOctaves = 4;
    int nOctaveLayers = 3;
    double hessianThreshold=100;
    bool extended = false;
    bool upright = false;

    try
    {
        read_int_param("surf_descriptor_extraction", nOctaves);
        read_int_param("surf_descriptor_extraction", nOctaveLayers);
        read_double_param("surf_descriptor_extraction", hessianThreshold);
        read_bool_param("surf_descriptor_extraction", extended);
        read_bool_param("surf_descriptor_extraction", upright);

        auto keypoints_1 = result_obj.get_keypoints();

        cv::Ptr<cv::Feature2D> extractor = cv::xfeatures2d::SURF::create(hessianThreshold,
                                                                         nOctaves,
                                                                         nOctaveLayers,
                                                                         extended,
                                                                         upright);

        cv::Mat descriptors_object;
        extractor->compute( in, keypoints_1, descriptors_object );

        result_obj.add_descriptor(descriptors_object);
    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool fast_feature_detection( Mat & in, Mat & out,
                             const algo_pipeline_params & params,
                             computed_objects & result_obj )
{
    int threshold=10;
    int type=FastFeatureDetector::TYPE_9_16;
    bool nonmaxSuppression=true;

    try
    {
        read_int_param("fast_feature_detection", threshold);
        read_int_param("fast_feature_detection", type);
        read_bool_param("fast_feature_detection", nonmaxSuppression);

        Ptr<FastFeatureDetector> detector = FastFeatureDetector::create(threshold, nonmaxSuppression, type);

        std::vector<KeyPoint> keypoints_1;

        detector->detect( in, keypoints_1 );

        for(auto keypoint : keypoints_1){
            result_obj.add_keypoint(keypoint);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool orb_feature_detection( Mat & in, Mat & out,
                            const algo_pipeline_params & params,
                            computed_objects & result_obj )
{
    int nfeatures=500;
    int nlevels=8;
    int edgeThreshold=31;
    int firstLevel=0;
    int wta_k=2;
    int scoreType=ORB::HARRIS_SCORE;
    int patchSize=31;
    int fastThreshold=20;
    double scaleFactor=1.2f;

    try
    {
        read_int_param("orb_feature_detection", nfeatures);
        read_int_param("orb_feature_detection", nlevels);
        read_int_param("orb_feature_detection", edgeThreshold);
        read_int_param("orb_feature_detection", firstLevel);
        read_int_param("orb_feature_detection", wta_k);
        read_int_param("orb_feature_detection", scoreType);
        read_int_param("orb_feature_detection", patchSize);
        read_int_param("orb_feature_detection", fastThreshold);
        read_double_param("orb_feature_detection", scaleFactor);

        cv::Ptr<cv::FeatureDetector> ORB = cv::ORB::create(nfeatures,
                                                           scaleFactor,
                                                           nlevels,
                                                           edgeThreshold,
                                                           firstLevel,
                                                           wta_k,
                                                           scoreType,
                                                           patchSize,
                                                           fastThreshold);
        std::vector<cv::KeyPoint> keypoints_1;

        ORB->detect( in, keypoints_1 );

        for(auto keypoint : keypoints_1){
            result_obj.add_keypoint(keypoint);
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool orb_descriptor_extraction( Mat & in, Mat & out,
                                const algo_pipeline_params & params,
                                computed_objects & result_obj )
{
    int nfeatures=500;
    int nlevels=8;
    int edgeThreshold=31;
    int firstLevel=0;
    int WTA_K=2;
    int scoreType=ORB::HARRIS_SCORE;
    int patchSize=31;
    int fastThreshold=20;
    double scaleFactor=1.2f;
    try
    {
        read_int_param("orb_descriptor_extraction", nfeatures);
        read_int_param("orb_descriptor_extraction", nlevels);
        read_int_param("orb_descriptor_extraction", edgeThreshold);
        read_int_param("orb_descriptor_extraction", firstLevel);
        read_int_param("orb_descriptor_extraction", WTA_K);
        read_int_param("orb_descriptor_extraction", scoreType);
        read_int_param("orb_descriptor_extraction", patchSize);
        read_int_param("orb_descriptor_extraction", fastThreshold);
        read_double_param("orb_descriptor_extraction", scaleFactor);

        auto keypoints_1 = result_obj.get_keypoints();

        cv::Ptr<cv::FeatureDetector> extractor = cv::ORB::create(nfeatures,
                                                                 scaleFactor,
                                                                 nlevels,
                                                                 edgeThreshold,
                                                                 firstLevel,
                                                                 WTA_K,
                                                                 scoreType,
                                                                 patchSize,
                                                                 fastThreshold);
        cv::Mat descriptors_object;
        extractor->compute( in, keypoints_1, descriptors_object );

        result_obj.add_descriptor(descriptors_object);
    }
    catch(...)
    {
        return false;
    }

    return true;
}


bool brief_descriptor_extraction( Mat & in, Mat & out,
                                  const algo_pipeline_params & params,
                                  computed_objects & result_obj )
{
    int bytes = 32;
    bool use_orientation = false;

    try
    {
        read_int_param("brief_descriptor_extraction", bytes);
        read_bool_param("brief_descriptor_extraction", use_orientation);

        auto keypoints_1 = result_obj.get_keypoints();

        cv::Ptr<cv::Feature2D> extractor;
        extractor = cv::xfeatures2d::BriefDescriptorExtractor::create(bytes, use_orientation);

        cv::Mat descriptors_object;
        extractor->compute( in, keypoints_1, descriptors_object );

        result_obj.add_descriptor(descriptors_object);
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool harris_corner_detection( Mat & in, Mat & out,
                              const algo_pipeline_params & params,
                              computed_objects & result_obj )
{
    int blockSize=2;
    int ksize = 3;
    int borderType = BORDER_DEFAULT;
    int norm_type = NORM_MINMAX;
    int dtype = CV_32FC1;
    int alpha = 0;
    int beta = 255;
    int thresh = 200;
    double k = 0.04;

    try
    {
        read_int_param("harris_corner_detection", blockSize);
        read_int_param("harris_corner_detection", ksize);
        read_int_param("harris_corner_detection", borderType);
        read_int_param("harris_corner_detection", norm_type);
        read_int_param("harris_corner_detection", dtype);
        read_int_param("harris_corner_detection", alpha);
        read_int_param("harris_corner_detection", beta)
        read_int_param("harris_corner_detection", thresh);
        read_double_param("harris_corner_detection", k);

        Mat dst, dst_norm, dst_norm_scaled;

        dst = Mat::zeros( in.size(), CV_32FC1 );

        cornerHarris( in, dst, blockSize, ksize, k, borderType );

        normalize( dst, dst_norm, alpha, beta, norm_type, dtype, Mat() );

        convertScaleAbs( dst_norm, dst_norm_scaled );

        for( int j = 0; j < dst_norm.rows ; j++ )
        { for( int i = 0; i < dst_norm.cols; i++ )
            {
                if( dst_norm.at<float>(j,i) > thresh )
                {
                    result_obj.add_corner(Point2f(i,j));
                }
            }
        }
    }
    catch(...)
    {
        return false;
    }

    return true;
}

bool shi_tomasi_corner_detection( Mat & in, Mat & out,
                                  const algo_pipeline_params & params,
                                  computed_objects & result_obj )
{
    int maxCorners = 23;
    int blockSize = 3;
    bool useHarrisDetector = false;
    double k = 0.04;
    double qualityLevel = 0.01;
    double minDistance = 10;

    try
    {
        read_int_param("shi_tomasi_corner_detection", maxCorners);
        read_int_param("shi_tomasi_corner_detection", blockSize);
        read_bool_param("shi_tomasi_corner_detection", useHarrisDetector);
        read_double_param("shi_tomasi_corner_detection", k);
        read_double_param("shi_tomasi_corner_detection", qualityLevel);
        read_double_param("shi_tomasi_corner_detection", minDistance);

        vector<Point2f> corners;

        goodFeaturesToTrack( in,
                             corners,
                             maxCorners,
                             qualityLevel,
                             minDistance,
                             Mat(),
                             blockSize,
                             useHarrisDetector,
                             k );

        for(auto corner : corners){
            result_obj.add_corner(corner);
        }

    }
    catch(...)
    {
        return false;
    }

    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/feature_extraction.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/draw_objects.cpp
// //////////////////////////////////////////////////////////////////////



namespace cm {

namespace libopencv {

bool draw_circles( Mat & in, Mat & out,
                   const algo_pipeline_params & params,
                   computed_objects & result_obj )
{
    int line_thickness = 1;

    try
    {
        read_int_param("draw_circles", line_thickness);

        CM_LOG << "Number of objects to draw = " <<
                  result_obj.get_rectangles().size() << std::endl;

        for(auto &c : result_obj.get_rectangles())
        {
            int c_radius = ( c.width + c.height ) / 2;
            cv::Point c_centre(c.origin.x + c.width/2 , c.origin.y + c.height/2);
            cv::circle( out,
                        c_centre,
                        c_radius,
                        k_pen_color,
                        line_thickness);
        }
    }
    catch(...)
    {
        return false;
    }


    return true;
}

bool draw_rectangles( Mat & in, Mat & out,
                      const algo_pipeline_params & params,
                      computed_objects & result_obj )
{
    int line_thickness = 1;

    try
    {
        read_int_param("draw_rectangles", line_thickness);

        auto found_rectangles = result_obj.get_rectangles();
        CM_LOG << "Number of rectangles = " << result_obj.get_rectangles().size()
               << std::endl;

        for(int i=0; i<found_rectangles.size();i++)
        {
            cv::rectangle(out,
                          cm::get_cv_rect(found_rectangles[i]),
                          cv::Scalar(255,255,255),
                          line_thickness);
        }
    }
    catch(...)
    {
        return false;
    }


    return true;
}

bool draw_labelled_rectangles(Mat &in, Mat &out,
                              const algo_pipeline_params &params,
                              computed_objects &result_obj)
{
    int line_thickness = 1;

    try
    {
        read_int_param("draw_rectangles", line_thickness);

        auto found_rectangles = result_obj.dnn_obj.dnn_results;
        CM_LOG << "Number of labelled rectangles = " << found_rectangles.size()
               << std::endl;

        for(int i=0; i<found_rectangles.size();i++)
        {
            auto rect_cv = cm::get_cv_rect(found_rectangles[i].object_rectangle);

            cv::rectangle(out,
                          rect_cv,
                          cv::Scalar(255,255,255),
                          line_thickness);

            cv::putText(out,
                        found_rectangles[i].object_label.c_str(),
                        rect_cv.tl(),
                        FONT_HERSHEY_SIMPLEX, 0.8,
                        cv::Scalar(255,255,255));
        }
    }
    catch(...)
    {
        return false;
    }


    return true;
}

bool draw_keypoints(Mat &in, Mat &out,
                    const algo_pipeline_params &params,
                    computed_objects &result_obj)
{
    int matcher_flag = 0;
    try
    {
        read_int_param("draw_keypoints", matcher_flag);

        CM_LOG << "Number of keypoints = " << result_obj.get_keypoints().size()
               << std::endl;

        auto keypoints = result_obj.get_keypoints();

        drawKeypoints( out, keypoints, out, k_pen_color, matcher_flag );
    }
    catch(...)
    {
        return false;
    }


    return true;
}

bool draw_corners(Mat &in, Mat &out,
                  const algo_pipeline_params &params,
                  computed_objects &result_obj)
{
    int radius = 4;
    int line_thickness = 1;

    try
    {
        read_int_param("draw_corners", radius);
        read_int_param("draw_corners", line_thickness);

        auto corners = result_obj.get_corners();

        CM_LOG <<"Number of corners detected = "<<corners.size()<<std::endl;

        for( int i = 0; i < corners.size(); i++ )
        {
            cv::circle( out,
                        corners[i],
                        radius,
                        k_pen_color,
                        line_thickness);
        }

    }
    catch(...)
    {
        return false;
    }


    return true;
}

}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/draw_objects.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/libopencv/facial_analysis.cpp
// //////////////////////////////////////////////////////////////////////



namespace cm{

namespace libopencv{

bool caffe_model_loader(Mat & in, Mat & out,
                        const algo_pipeline_params & params,
                        computed_objects & result_obj){


    try
    {

        result_obj.clear_models();
        result_obj.clear();

        string model_prototxt_file , caffe_model_file;
        string emotions_class_file;

        read_string_param("caffe_model_loader", model_prototxt_file);
        read_string_param("caffe_model_loader", caffe_model_file);
        read_string_param("caffe_model_loader", emotions_class_file);

        cv::String model_configuration
                = cv::String(model_prototxt_file.c_str());
        cv::String model_weights
                = cv::String(caffe_model_file.c_str());

        result_obj.dnn_obj.dnn_net = cv::dnn::readNetFromCaffe(model_prototxt_file,
                                                                 caffe_model_file);
        if(result_obj.dnn_obj.dnn_net.empty())
            return false;
        else
        {
            ifstream class_names_file_obj(emotions_class_file);
            if (class_names_file_obj.is_open())
            {
                string name_item = "";
                while (std::getline(class_names_file_obj, name_item))
                    result_obj.dnn_obj.dnn_class_names.push_back(name_item);
            }
            class_names_file_obj.close();

            return true;
        }
    }
    catch(...)
    {
        return false;
    }
    return true;
}


bool facial_analysis(Mat & in, Mat & out,
                        const algo_pipeline_params & params,
                        computed_objects & result_obj)

{

    try {
        string face_extraction_cascade_file;
        std::vector<double> mean_values = {0, 0, 0};
        std::vector<Rect> faces;
        read_string_param("facial_analysis", face_extraction_cascade_file);
        read_vector_double_param("facial_analysis", mean_values);


        if (mean_values.size() != 3){
            mean_values = {0, 0, 0};
        }

        cv::String cascade_file
                = cv::String(face_extraction_cascade_file.c_str());


        CascadeClassifier faceCascade = CascadeClassifier(cascade_file);
        Mat gray;
        cvtColor(in, gray, COLOR_BGR2GRAY);
        faceCascade.detectMultiScale(gray, faces);

        cout << mean_values[0] << "," << mean_values[1] << ", "<< mean_values[2]<<endl;
         for (auto rect: faces)

         {

             cout << rect.x << ", " << rect.y << "," << rect.width << "," <<rect.height <<endl;
//            try {


//            if ( rect.width * 1.75 < in.cols){
//                rect.width = rect.width * 1.75;

//            }

//            if ( rect.height * 1.75 < in.rows){
//                rect.height= rect.height * 1.75;

//            }

//            if ((rect.x - rect.width/4) > 0) {
//                rect.x = rect.x - rect.width/4;

//            }
//            if ((rect.y - rect.height/4) > 0) {
//                rect.y = rect.y - rect.height/4;

//            }
//             }

//             catch(...){

//             }
            cout << rect.x << ", " << rect.y << "," << rect.width << "," <<rect.height<<endl;

            cv::Mat face = in(rect);
            Mat inputBlob = cv::dnn::blobFromImage(face,
                                                   1.0f  ,
                                                   Size(256, 256),
                                                   Scalar(mean_values[0], mean_values[1], mean_values[2]), true, false);

            result_obj.dnn_obj.dnn_net.setInput(inputBlob);

            Mat detectionMat = result_obj.dnn_obj.dnn_net.forward();

            cout <<"Output Size" << detectionMat.size<< endl;
            std::vector<float> detected_scores;
            for (int col=0; col < detectionMat.cols; col++)
            {
                cout << detectionMat.at<float>(0, col);
                detected_scores.push_back(detectionMat.at<float>(0, col));
            }
            int index = distance(detected_scores.begin(), max_element(detected_scores.begin(), detected_scores.end()));
            dnn_result result_item;
            if(index < result_obj.dnn_obj.dnn_class_names.size())
            {
                result_item.object_label =
                        result_obj.dnn_obj.dnn_class_names[index];
            }
            result_item.object_rectangle = cm::rect_2_bounding_rect(rect);
            result_obj.dnn_obj.dnn_results.push_back(result_item);
        }

    }
    catch (...){

        return false;
    }
    return true;


}




}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/libopencv/facial_analysis.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/function_table.cpp
// //////////////////////////////////////////////////////////////////////




cm::function_table::function_table()
{
    add_algo_block_function(difference_of_gaussian)
    add_algo_block_function(gaussian_blur);
    add_algo_block_function(median_blur);
    add_algo_block_function(bilateral_filter);
    add_algo_block_function(otsu_binarization);
    add_algo_block_function(convert_color_to_gray);
    add_algo_block_function(canny_edges);
    add_algo_block_function(box_filter_blur);
    add_algo_block_function(laplacian_operator);
    add_algo_block_function(sobel_operator);

    add_algo_block_function(sift_feature_detection);
    add_algo_block_function(sift_descriptor_extraction);
    add_algo_block_function(surf_feature_detection);
    add_algo_block_function(surf_descriptor_extraction);
    add_algo_block_function(fast_feature_detection);
    add_algo_block_function(orb_feature_detection);
    add_algo_block_function(orb_descriptor_extraction);

    add_algo_block_function(brief_descriptor_extraction);
    add_algo_block_function(harris_corner_detection);
    add_algo_block_function(shi_tomasi_corner_detection);

    add_algo_block_function(resize);
    add_algo_block_function(set_region_of_interest);
    add_algo_block_function(translate_result);

    add_algo_block_function(circle_finder);
    add_algo_block_function(rectangle_finder);
    add_algo_block_function(yolo_model_loader);
    add_algo_block_function(yolo_object_finder);

    add_algo_block_function(caffe_model_loader);
    add_algo_block_function(facial_analysis);


    add_algo_block_function(draw_circles);
    add_algo_block_function(draw_rectangles);
    add_algo_block_function(draw_labelled_rectangles);
    add_algo_block_function(draw_keypoints);
    add_algo_block_function(draw_corners);

    add_algo_block_function(geometric_filtering);
}

cm::function_table &cm::function_table::instance()
{
    static cm::function_table global_function_table;

    return global_function_table;
}

cm::algo_block_fptr
cm::function_table::get_algo_block_function(const std::string &function_name)
{
    auto table_instance = function_table::instance();
    auto function_item = table_instance.algo_block_functions.find(function_name);
    if(function_item != table_instance.algo_block_functions.end())
        return function_item->second;
    else
        return nullptr;
}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/function_table.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/simple_finder_pipeline.cpp
// //////////////////////////////////////////////////////////////////////


namespace cm {

simple_finder_pipeline::simple_finder_pipeline()
{
    finder_process = new process_block();

    input_image = output_image = nullptr;
}

simple_finder_pipeline::~simple_finder_pipeline()
{
    delete finder_process;

    delete input_image;

    delete output_image;
}

bool simple_finder_pipeline::set_input_image(const cv::Mat & in_image)
{
    if(input_image != nullptr)
        delete input_image;

    input_image = new cm::cv_image_holder(in_image);

    return input_image->is_valid();
}

bool simple_finder_pipeline::set_output_image(const cv::Mat & out_image)
{
    if(output_image != nullptr)
        delete output_image;

    output_image = new cm::cv_image_holder(out_image);

    return output_image->is_valid();
}

Mat simple_finder_pipeline::get_output_image()
{
    return output_image->get_cv_matrix();
}

bool simple_finder_pipeline::set_pipeline_configuration(const std::string & json_buffer)
{
    return finder_process->set_process_block_parameters(json_buffer,
                                                 "simple_finder_pipeline");
}

bool simple_finder_pipeline::process_pipeline()
{
    finder_process->reset();

    if ( finder_process->set_input_image(input_image) == false )
    {
        CM_LOG << "Error setting input image for simple finder process \n";
        return false;
    }

    if ( finder_process->set_output_image(output_image) == false )
    {
        CM_LOG << "Error setting output image for simple finder process \n";
        return false;
    }

    return finder_process->process_image();
}


bool simple_finder_pipeline::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      int value)
{
    return finder_process->set_parameter(block_name, param_name, value);
}

bool simple_finder_pipeline::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      double value)
{
    return finder_process->set_parameter(block_name, param_name, value);
}

bool simple_finder_pipeline::set_parameter(const std::string &block_name,
                                      const std::string &param_name,
                                      std::string value)
{
    return finder_process->set_parameter(block_name, param_name, value);
}

}

// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/simple_finder_pipeline.cpp
// //////////////////////////////////////////////////////////////////////






// //////////////////////////////////////////////////////////////////////
// Beginning of content of file: ../../cpp/tracker_pipeline.cpp
// //////////////////////////////////////////////////////////////////////


cm::feature_detector_comparator::feature_detector_comparator() :
    ratio_test_value(0.8f), ransac_threshold(2.5f)
{
    feature_detector = cv::ORB::create(500, 1.5, 4);
    feature_extractor = cv::ORB::create(500, 1.5, 4);
    descriptor_matcher = cv::makePtr<cv::BFMatcher>((int)cv::NORM_HAMMING, false);
}

cm::feature_detector_comparator::~feature_detector_comparator()
{
}

void cm::feature_detector_comparator::compute_keypoints(image_type t, const Mat &img)
{
    if(t == reference_image)
    {
        feature_detector->detect(img, ref_kp);
    }

    if(t == test_image)
    {
        feature_detector->detect(img, test_kp);
    }
}

void cm::feature_detector_comparator::compute_descriptors(image_type t, const Mat &img)
{
    if(t == reference_image)
    {
        feature_extractor->compute(img, ref_kp, ref_desc);
    }

    if(t == test_image)
    {
        feature_extractor->compute(img, test_kp, test_desc);
    }
}

void cm::feature_detector_comparator::compute_matches()
{
    descriptor_matcher->knnMatch(ref_desc, test_desc, ref_test_matches, 2);

    for(unsigned i = 0; i < ref_test_matches.size(); i++)
    {
        if(ref_test_matches[i][0].distance < ratio_test_value * ref_test_matches[i][1].distance)
        {
            ref_matches.push_back(ref_kp[ref_test_matches[i][0].queryIdx]);
            test_matches.push_back(test_kp[ref_test_matches[i][0].trainIdx]);
        }
    }
}

bool cm::feature_detector_comparator::find_matched_bounding_box()
{
    Mat inlier_mask, homography;
    vector<KeyPoint> inliers1, inliers2;
    vector<DMatch> inlier_matches;

    vector<cv::Point2f> ref_match_points, test_match_points;
    KeyPoint::convert(ref_matches, ref_match_points);
    KeyPoint::convert(test_matches, test_match_points);

    if(ref_matches.size() >= 4) {
        homography = findHomography(ref_match_points, test_match_points,
                                    RANSAC, ransac_threshold, inlier_mask);
    }

    if(ref_matches.size() < 4 || homography.empty()) {
        return false;
    }

    for(unsigned i = 0; i < ref_matches.size(); i++) {
        if(inlier_mask.at<uchar>(i)) {
            int new_i = static_cast<int>(inliers1.size());
            inliers1.push_back(ref_matches[i]);
            inliers2.push_back(test_matches[i]);
            inlier_matches.push_back(DMatch(new_i, new_i, 0));
        }
    }

    perspectiveTransform(ref_bbox, test_bbox, homography);
    return true;
}

void cm::feature_detector_comparator::set_reference_image(const cv::Mat & ref)
{
    compute_keypoints(reference_image, ref);

    compute_descriptors(reference_image, ref);

    ref_bbox.push_back(cv::Point(0,0));
    ref_bbox.push_back(cv::Point(ref.cols, 0));
    ref_bbox.push_back(cv::Point(ref.cols, ref.rows));
    ref_bbox.push_back(cv::Point(0,ref.rows));
}

void cm::feature_detector_comparator::set_test_image(const cv::Mat & test)
{
    test_kp.clear();
    ref_matches.clear();
    test_matches.clear();
    ref_test_matches.clear();

    compute_keypoints(test_image, test);

    compute_descriptors(test_image, test);
}

cm::tracker_pipeline::tracker_pipeline()
{
    input_image = output_image = reference_image = nullptr;
}

cm::tracker_pipeline::~tracker_pipeline()
{
    delete input_image;

    delete output_image;
}

bool cm::tracker_pipeline::set_input_image(const cv::Mat & in_image)
{
    if(input_image != nullptr)
        delete input_image;

    input_image = new cm::cv_image_holder(in_image);

    return input_image->is_valid();
}

bool cm::tracker_pipeline::set_output_image(const cv::Mat & out_image)
{
    if(output_image != nullptr)
        delete output_image;

    output_image = new cm::cv_image_holder(out_image);

    return output_image->is_valid();
}

bool cm::tracker_pipeline::set_reference_image(const cv::Mat & ref_image)
{
    if(reference_image != nullptr)
        delete reference_image;

    reference_image = new cm::cv_image_holder(ref_image);

    feature_detector_comparator_obj.set_reference_image(ref_image);

    return true;
}

bool cm::tracker_pipeline::process_pipeline()
{
    if(input_image == nullptr)
        return false;

    cv::Mat input_matrix = input_image->get_cv_matrix();
    cv::Mat output_matrix = output_image->get_cv_matrix();

    if(input_matrix.data == nullptr)
        return false;

    cm::libopencv::time_counter tc;
    feature_detector_comparator_obj.set_test_image(input_matrix);
    feature_detector_comparator_obj.compute_matches();
    feature_detector_comparator_obj.find_matched_bounding_box();

    cm::libopencv::drawBoundingBox(output_matrix, feature_detector_comparator_obj.test_bbox);

    return true;
}


// //////////////////////////////////////////////////////////////////////
// End of content of file: ../../cpp/tracker_pipeline.cpp
// //////////////////////////////////////////////////////////////////////





