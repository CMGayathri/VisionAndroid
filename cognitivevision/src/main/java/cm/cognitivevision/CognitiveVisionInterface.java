package cm.cognitivevision;

import android.content.Context;
import android.graphics.Bitmap;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

/**
 * Created by gayathris on 21/02/18.
 */

public class CognitiveVisionInterface {
    private Bitmap mBitmap;
    private Context mContext;

    private String SHAPEFINDER_CIRCLE_CFG = "";
    private String SHAPEFINDER_LINE_CFG = "";
    private String SHAPEFINDER_BOX_CFG = "";
    private String EDGE_DETECTION_HARRIS_CFG = "";
    private String EDGE_DETECTION_CANNY_CFG = "";
    private String EDGE_DETECTION_TOMASI_CFG = "";
    private String FEATURE_DETECTION_ORB_CFG = "";
    private String FEATURE_DETECTION_FAST_CFG = "";
    private String FEATURE_DETECTION_BRIEF_CFG = "";
    private String FEATURE_DETECTION_SIFT_CFG = "";
    private String FEATURE_DETECTION_SURF_CFG = "";
    private String PRE_PROCESS_SOBEL_CFG = "";
    private String PRE_PROCESS_LAPLACE_CFG = "";
    private String PRE_PROCESS_GAUSSIAN_CFG = "";

    private native void cognitiveVisionFinder(long addrRgba);
    private native void setConfiguration(String json);

    static {
        System.loadLibrary("cm-lib");
    }

    public CognitiveVisionInterface(Context ctx) {
        mContext = ctx;
        loadConfigurationFiles();
    }

    private void loadConfigurationFiles() {
        SHAPEFINDER_CIRCLE_CFG = readFile("circle_finder.json");
        EDGE_DETECTION_HARRIS_CFG = readFile("harris_corner_detection.json");
        EDGE_DETECTION_CANNY_CFG = readFile("canny_filter.json");
        EDGE_DETECTION_TOMASI_CFG = readFile("shi_tomasi_corner_detection.json");
        FEATURE_DETECTION_ORB_CFG = readFile("orb_feature_detection.json");
        FEATURE_DETECTION_FAST_CFG = readFile("fast_feature_detection.json");
        FEATURE_DETECTION_SIFT_CFG = readFile("sift_feature_detection.json");
        FEATURE_DETECTION_SURF_CFG = readFile("surf_feature_detection.json");
        PRE_PROCESS_SOBEL_CFG = readFile("sobal_filter.json");
        PRE_PROCESS_LAPLACE_CFG = readFile("laplace_filter.json");
    }

    public void setInputImage(Bitmap mBtimap) {
        this.mBitmap = mBtimap;
    }

    public Bitmap getOutputImage() {
        return this.mBitmap;
    }

    private void coreDetection() {
        Mat mat = new Mat(mBitmap.getWidth(), mBitmap.getHeight(), CvType.CV_8UC1);
        org.opencv.android.Utils.bitmapToMat(mBitmap, mat);
        cognitiveVisionFinder(mat.getNativeObjAddr());
        org.opencv.android.Utils.matToBitmap(mat, mBitmap);
    }

    public void shapeFinderCircles() {
        setConfiguration(SHAPEFINDER_CIRCLE_CFG);
        coreDetection();
    }

    public void edgeDetectionHarris() {
        setConfiguration(EDGE_DETECTION_HARRIS_CFG);
        coreDetection();
    }

    public void edgeDetectionCanny() {
        setConfiguration(EDGE_DETECTION_CANNY_CFG);
        coreDetection();
    }

    public void edgeDetectionShiTomasi() {
        setConfiguration(EDGE_DETECTION_TOMASI_CFG);
        coreDetection();
    }

    public void featureDetectionORB() {
        setConfiguration(FEATURE_DETECTION_ORB_CFG);
        coreDetection();
    }

    public void featureDetectionFAST() {
        setConfiguration(FEATURE_DETECTION_FAST_CFG);
        coreDetection();
    }

    public void featureDetectionSIFT() {
        setConfiguration(FEATURE_DETECTION_SIFT_CFG);
        coreDetection();
    }

    public void featureDetectionSURF() {
        setConfiguration(FEATURE_DETECTION_SURF_CFG);
        coreDetection();
    }

    public void preProcessFilterSobel() {
        setConfiguration(PRE_PROCESS_SOBEL_CFG);
        coreDetection();
    }

    public void preProcessFilterLaplace() {
        setConfiguration(PRE_PROCESS_LAPLACE_CFG);
        coreDetection();
    }

    private String readFile(String filename) {
        InputStream stream = null;
        try {
            stream = mContext.getAssets().open(filename);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return "";
    }
}
